**ublock-static-filters**

This repository is expected to only ever contain two files: `ublock-static-filters.txt` and this 
README. 
Copy the contents of the former into the "My filters" tab of your uBlock, hit "Apply changes" and watch the 
magic happen.

Warning: not for everybody. These filters reflect how *I* prefer to browse certain sites, and may remove 
functionality that you want. Use with discretion.

Supported sites:

* Abandonia
* Demonoid
* Discord
* Emuparadise
* Facebook buttons and comment sections
* GameFAQs
* Google+ buttons
* GSMArena
* HTPCGuides
* Imgur
* IPT
* KnowYourMeme
* MoparScape
* Nexus Mods
* Outlook
* Pastebin
* powerbot
* Reddit
* Rune-Server
* RYM
* SkyrimCalculator
* SourceForge
* V3
* Wikia
* Wikipedia
* YouTube

Others may be undocumented.
