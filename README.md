**ublock-static-filters**

This repository is expected to only ever contain two files: `ublock-static-filters.txt` and this 
README. 
Copy the contents of the former into the "My filters" tab of your uBlock, hit "Apply changes" and watch the 
magic happen.

Warning: not for everybody. These filters reflect how *I* prefer to browse certain sites, and may remove 
functionality that you want. Use with discretion.

Supported sites:

 * Imgur
 * IPT
 * Wikipedia
 * SourceForge
 * Abandonia
 * Facebook like boxes/buttons
 * Google+ boxes/buttons
 * GSMArena
 * Wikia
 * V3
 * HTPCGuides
 * Outlook
 * YouTube
 * powerbot
 * Pastebin
 * Emuparadise
 * Reddit
 * SkyrimCalculator
 * RYM
 * Demonoid
 * Rune-Server

Others may be undocumented.
